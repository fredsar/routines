package sched

import (
	"fmt"
	"runtime"
)

func POCWithSched() {
	go say("america")
	say("europa")
}

func say(s string) {
	for i := 0; i < 5; i++ {
		runtime.Gosched()
		fmt.Println(s)
	}
}