package sleep

import (
	"fmt"
	"time"
)

func POCWithSleep() {
	go say("hola")
	say("adios")
}

func say(s string) {
	for i := 0; i < 5; i++ {
		time.Sleep(100 * time.Millisecond)
		fmt.Println(s)
	}
}
