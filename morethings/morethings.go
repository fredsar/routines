package morethings

import (
	"fmt"
	"sync"
)

func POCUsandoVariasCosas() {
	wg := sync.WaitGroup{}
	wg.Add(1)
	errChan := make(chan error)
	doneWgChan := make(chan bool)

	go func() {
		defer wg.Done()
		err := say("cual es el numero prohibido?")
		if err != nil {
			errChan <- err
		}
	}()

	go func() {
		wg.Wait()
		close(doneWgChan) // <- doneWgChan ya ha sido cerrado
	}()

	select {
	case <-doneWgChan: // <- doneWgChan ya ha sido cerrado
		break
	case err := <-errChan:
		close(errChan)
		fmt.Println(err)
		return
	}
	fmt.Println("Hemos terminado con éxito")
}

func say(s string) error {
	for i := 0; i < 5; i++ {
		/*s1 := rand.NewSource(time.Now().Unix())
		r := rand.New(s1)
		if r.Intn(10) == 4 {*/
		if i == 4 {
			return fmt.Errorf("llegaste a 4, el numero prohibido")
		}
		fmt.Println(s)
	}
	return nil
}
