package waitgroup

import (
	"fmt"
	"sync"
)

func POCWithWG() {
	wg := sync.WaitGroup{}
	wg.Add(1)

	go func() {
		say("viernes")
		wg.Done()
	}()
	wg.Wait()
}

func say(s string) {
	for i := 0; i < 5; i++ {
		fmt.Println(s)
	}
}
