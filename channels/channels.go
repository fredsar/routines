package channels

import "fmt"

func POCWithChannels() {
	chanErr := make(chan error)
	go func() {
		say("golang en español")
		chanErr <- fmt.Errorf("ya no hay elementos")
	}()
	select {
	case err := <-chanErr:
		close(chanErr)
		fmt.Println(err)
		return
	}
}

func say(s string) {
	for i := 0; i < 5; i++ {
		fmt.Println(s)
	}
}
